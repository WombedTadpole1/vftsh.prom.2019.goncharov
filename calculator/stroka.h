//
// Created by Женёк on 29.12.2019.
//

#ifndef STROKA_STROKA_H
#define STROKA_STROKA_H

#include <string>
#include <iostream>
#include <vector>

using std::string;
using std::cout;
using std::vector;

class TStroka {
private:
    int Pow(int a, int n) {
        if(n == 0) {
            return 1;
        } else if(n == 1) {
            return a;
        } else {
            return a * Pow(a, n - 1);
        }
    }

    bool IsZnak(char z) {
        char znaki[4] = {'+','-','*','/'};
        for (int i = 0; i < 4; ++i) {
            if (z == znaki[i]) {
                return true;
            }
        }
        return false;
    }

    int SearchZnak() {
        int znak;
        for (int i = stroka.size() - 2; i > 0; --i) {
            if (IsZnak(stroka[i]) && !IsZnak(stroka[i - 1])) {
                znak = i;
            }
        }
        return znak;
    }

    int DefineNumber(int first, int second) {
        char numbers[10] = { '0','1','2','3','4','5','6','7','8','9'};
        int st = 0;
        int number = 0;
        for (int i = second; i >= first ; --i) {
            if (!IsZnak(stroka[i]) ) {
                for (int n = 0; n <= 10; ++n) {
                    if (stroka[i] == numbers[n]) {
                        number = number + n * Pow(10,st);
                        ++st;
                    }
                }
            }
            if (IsZnak(stroka[i]) && stroka[i] == '-') {
                number = -1 * number;
            }
        }
        return number;
    }

    int GetResult(int num1, int num2, char znak) {
        int res;
        switch (stroka[znak]) {
            case '-':
                res = num1 - num2;
                break;
            case '+':
                res = num1 + num2;
                break;
            case '*':
                res = num1 * num2;
                break;
            case '/':
                res = num1 / num2;
                break;
        }
        return res;
    }

public:
    vector<char> stroka;
    TStroka() {}

    void LoadStroka(string a) {
        for (int i = 0; i < a.size(); ++i) {
            if (a[i] != ' '){
                stroka.push_back(a[i]);
            }
        }

    }


    void StrokaCalculate() {
        int number_1;
        int number_2;
        int znak = SearchZnak();
        number_1 = DefineNumber(0, znak - 1);
        number_2 = DefineNumber(znak + 1, stroka.size() - 1);
        cout << " result = " << GetResult( number_1, number_2, znak) << "\n";
    }

};


#endif //STROKA_STROKA_H
