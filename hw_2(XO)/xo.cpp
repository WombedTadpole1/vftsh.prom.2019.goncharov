#include <iostream>

class TXO {
public:
    char array[4][4] = {{'0', '1', '2', '3'},{'1', '-', '-', '-'},{'2', '-', '-', '-'},{'3', '-', '-', '-'}};;
    TXO(){}

    void Print() {
        for (int j = 0; j < 4; ++j) {
            for (int i = 0; i < 4; ++i) {
                std::cout << array[j][i] << " ";
            }
            std::cout << "\n";
        }

    }

    void StepX( int x, int y){
        array[x][y] = 'X';
        Print();
    }
    void StepO( int x, int y){
        array[x][y] = 'O';
        Print();
    }
    int Checkall(int a) {
        char b[1];
        if (a == 1) {
            b[0] = 'X';
        }
        if (a == 0) {
            b[0] = 'O';
        }

        int sum;

        for (int k = 1; k < 4; ++k) {
            sum = 0;
            for (int l = 1; l < 4; ++l) {
                if (array[l][k] == b[0]) {
                    ++sum;
                }
            }
            if (sum == 3) {
                std::cout << "The winner is " << b[0] << "\n";
                return 1;
            }
        }
        for (int l = 1; l < 4; ++l) {
            sum = 0;
            for (int k = 1; k < 4; ++k) {
                if (array[l][k] == b[0]) {
                    ++sum;
                }
            }
            if (sum == 3) {
                std::cout << "The winner is " << b[0] << "\n";
                return 1;
            }
        }
        sum = 0;
        for (int l = 1; l < 4; ++l) {
            if (array[l][l] == b[0]) {
                ++sum;
            }
        }
        if (sum == 3) {
            std::cout << "The winner is " << b[0] << "\n";
            return 1;
        }
        sum = 0;
        for (int l = 1; l < 4; ++l) {
            if (array[l][4 - l] == b[0]) {
                ++sum;
            }
        }
        if (sum == 3) {
            std::cout << "The winner is " << b[0] << "\n";
            return 1;
        }

        return 0;
    }

    int Check(int x, int y, int a) {

        if ((x <= 0) || (x > 3) || (y <= 0) || (y > 3)) {
            std::cout << " Try again \n";
            Enter(a);
        } else if (array[x][y] == '-') {
            if (a == 1) {
                StepX(x, y);
                return Checkall(a);
            }
            if (a == 0) {
                StepO(x, y);
                return Checkall(a);
            }

        } else {
            std::cout << " Sorry, this place have been engaged \n";
            Enter(a);
        }
    }
    void Enter(int a){
        int x,y,win;
        for (int i = 0; i <= 9; ++i) {
            if (i == 9) {
                std::cout << "Draw \n";
                break;
            }
            std::cout << "Make your step please \n";
            std::cin >> x;
            std::cin >> y;
            win = Check(x, y, a);
            if (win == 1) {
                break;
            }
            if (a == 1) {
                a = 0;
            } else {
                a = 1;
            }
        }
    }
    void Preload(){
        int a;
        std::cout << "During the game enter coordinates (raw, column) \n";
        std::cout << "If you want play X enter 1, if Y enter 0 \n";
        std::cin >> a;
        if (a == 1) {
            std::cout << "You match X \n";
            Enter(a);
        }
        else if (a == 0) {
            std::cout << "You match O \n";
            Enter(a);
        }
        else {
            std::cout << " Try again \n";
            Preload();
        }
    }

};



int main() {

    TXO myxo;
    myxo.Preload();

    return 0;
}
