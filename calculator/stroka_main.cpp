#include <iostream>
#include "stroka.h"

using std::cout;
using std::string;
using std::cin;

int main()
{
    string a;
    cout << " Please enter string \n";
    getline(cin, a);
    TStroka mystroka;
    mystroka.LoadStroka(a);
    mystroka.StrokaCalculate();

    return 0;
}