//
// Created by Женёк on 29.12.2019.
//

#ifndef NEWXO_XO_H
#define NEWXO_XO_H
#include <iostream>
#include <vector>

using std::vector;

class XO {
public:
    char array[4][4] = {{'0', '1', '2', '3'},{'1', '-', '-', '-'},{'2', '-', '-', '-'},{'3', '-', '-', '-'}};;
    vector<int> PossibleSteps;
    bool bot = false;
    XO() {}

    void Print() {
        for (int j = 0; j < 4; ++j) {
            for(int i = 0; i < 4; ++i) {
                std::cout << array[j][i] << " ";
            }
            std::cout << "\n";
        }
    }
    void LoadPossibleSteps () {
        //clear old values in PossibleSteps if they existed
        if (PossibleSteps.size() != 0) {
            PossibleSteps.clear();
        }
        //then put in it new ones
        int el;
        for (int k = 1;k < 4;++k ) {
            for (int l = 1; l < 4; ++l) {
                if (array[l][k] == '-') {
                    el = l*10 + k;
                    PossibleSteps.push_back(el);

                }
            }
        }
    }

    int GetRandNumber(int min, int max) {
        static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
        return static_cast<int>(rand() * fraction * (max - min + 1) + min);
    }

    int ChoosePossibleStep() {
        LoadPossibleSteps();
        if ( PossibleSteps.size() != 0 ){
            int bot_step_coord = GetRandNumber( 0, PossibleSteps.size() -1 );
            return PossibleSteps[bot_step_coord];
        }
    }


    void StepX(int x, int y) {
        array[x][y] = 'X';
        Print();
    }
    void StepO(int x, int y) {
        array[x][y] = 'O';
        Print();
    }
    int CheckAll(int a) {
        char b[1];
        if (a == 1) {
            b[0] = 'X';
        }
        if (a == 0) {
            b[0] = 'O';
        }
        int sum;
        //check all row
        for (int k = 1;k < 4;++k ) {
            sum = 0;
            for (int l = 1; l < 4; ++l) {
                if(array[l][k] == b[0]) {
                    ++sum;
                }
            }
            if (sum == 3) {
                std::cout << "The winner is " << b[0] << "\n";
                return 1;
            }
        }
        //check all column
        for (int l = 1;l < 4;++l ) {
            sum = 0;
            for (int k = 1; k < 4; ++k) {
                if(array[l][k] == b[0]) {
                    ++sum;
                }
            }
            if (sum == 3) {
                std::cout << " The winner is " << b[0] << "\n";
                return 1;
            }
        }
        //check two diagonals
        sum = 0;
        for (int l = 1;l < 4;++l) {
            if(array[l][l] == b[0]) {
                ++sum;
            }
        }
        if (sum == 3) {
            std::cout << "The winner is " << b[0] << "\n";
            return 1;
        }
        sum = 0;
        for (int l = 1;l < 4;++l ) {
            if(array[l][4 - l] == b[0]){
                ++sum;
            }
        }
        if (sum == 3) {
            std::cout << "The winner is " << b[0] << "\n";
            return 1;
        }

        //check pair
        sum = 0;
        for (int k = 1; k < 4; ++k) {
            for (int l = 1; l < 4; ++l) {
                if (array[l][k] == '-') {
                    ++sum;
                }
            }
        }
        if (sum == 0) {
            std::cout << "The pair \n";
            return 1;
        }
        return 0;
    }

    int Check(int x, int y, int a) {
        int bot_step;
        if((x <= 0) || (x > 3) || (y <= 0) || (y > 3)) {
            std::cout << " Try again \n";
            Enter(a);
        } else if (array[x][y] == '-') {
            if (a == 1){
                StepX(x, y);
                return CheckAll(a);
            }
            if (a == 0) {
                StepO(x, y);
                return CheckAll(a);
            }
        } else {
            std::cout<<" Sorry, this place have been engaged \n";
            Enter(a);
        }
    }

    void Enter(int a) {
        int x, y, win, bot_step;
        if (bot) {
            if (a == 1) {
                for (int i = 0; i <= 5; ++i) {
                    std::cout << " Make your step please \n";
                    std::cin >> x;
                    std::cin >> y;
                    win = Check(x, y, 1);
                    if (win == 1) {
                        break;
                    }
                    std::cout << " Now my step \n";
                    bot_step = ChoosePossibleStep();
                    y = bot_step % 10;
                    x = ( bot_step - y ) / 10;
                    std::cout << " " << x << " " << y << "\n";
                    win = Check(x, y, 0);
                    if (win == 1) {
                        break;
                    }
                }
            } else {
                for (int i = 0; i <= 5; ++i) {
                    std::cout << " Now my step \n";
                    bot_step = ChoosePossibleStep();
                    y = bot_step % 10;
                    x = (bot_step - y) / 10;
                    std::cout << " " << x << " " << y << "\n";
                    win = Check(x, y, 1);
                    if (win == 1) {
                        break;
                    }
                    std::cout<<" Make your step please \n";
                    std::cin >>x;
                    std::cin >>y;
                    win = Check(x, y, 0);
                    if (win == 1) {
                        break;
                    }
                }
            }
        } else {
            for (int i = 0; i < 10; ++i) {

                std::cout << " Make your step please \n";
                std::cin >> x;
                std::cin >> y;
                win = Check(x, y, a);
                if (win == 1) {
                    break;
                }
                if( a == 1) {
                    a = 0;
                } else {
                    a = 1;
                }
            }

        }
    }

    void Preload() {
        int a,b;
        std::cout << " If you want play alone enter 1, if play with Bot enter 0 \n";
        std::cin >> b;
        if (b == 1) {
            std::cout << " OK play alone \n";
        } else if (b == 0) {
            std::cout << " I am bot, try to win me \n";
            bot = true;
        } else {
            std::cout << " Try again \n";
            Preload();
        }
        std::cout << " If you want play X enter 1, if play O enter 0 \n";
        std::cin >> a;
        if (a == 1) {
            std::cout << " You match X \n";
            Enter(a);
        } else if (a == 0) {
            std::cout << " You match O \n";
            Enter(a);
        } else {
            std::cout << " Try again \n";
            Preload();
        }
    }
};


#endif //NEWXO_XO_H
